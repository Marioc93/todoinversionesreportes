package reporte;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;

public class Conexion {
	
	public static final String url = "jdbc:mysql://localhost:3306/todoinversiones";
	public static final String username = "root";
	public static final String password = "pass";
	
	public Conexion() {
		
	}
	
	public static Connection getConection(){
		Connection con = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			System.out.println(e);
		}
		return con;
		
	}

}

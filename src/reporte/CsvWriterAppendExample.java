package reporte;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.mysql.jdbc.PreparedStatement;

import modelo.Cliente;
import modelo.Usuario;

public class CsvWriterAppendExample {
	
	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
	
	public static void main(String[] args) {
		crearCliente();
		crearCliente();
		crearCliente();
		crearExcel();
	}
	
	public static void crearCliente() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
		Usuario u = new Usuario("pepe", "lopez", "pepe@mail.com", "46512037");
		Cliente c = new Cliente(u, "Banco S.A.", true, "Moderador");
		
		entityManager.persist(u);
		entityManager.persist(c);
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	public static void crearExcel() {
		Workbook book = new HSSFWorkbook();
		Sheet sheet = book.createSheet("Reporte");
		
		try {
			
			String[] cabecera = new String[] {"Id","Nombre","Apellido","Dni","Email"};

			Row row = sheet.createRow(0);
			
			for (int i = 0; i < cabecera.length; i++) {
				Cell celdaEncabezado = row.createCell(i);
				celdaEncabezado.setCellValue(cabecera[i]);
			}
			
			Conexion con = new Conexion();
			PreparedStatement ps;
	        ResultSet rs;
	        Connection conn = con.getConection();
	        
	        ps = (PreparedStatement) conn.prepareStatement("SELECT id, nombre, apellido, dni, mail FROM usuario");
			
	        rs = ps.executeQuery();
	        
	        int numFilaDatos = 1;
	        
	        int numCol = rs.getMetaData().getColumnCount();
	        System.out.println(numCol);

			while(rs.next()) {
				Row filaDato = sheet.createRow(numFilaDatos);
				for (int a = 0; a < numCol; a++) {
					Cell celdaDato = filaDato.createCell(a);
					
//					if (a == 2) {
//						celdaDato.setCellValue(rs.getInt(a+1));
//					}else {
//						celdaDato.setCellValue(rs.getString(a+1));
//					}
					celdaDato.setCellValue(rs.getString(a+1));
				}				
				numFilaDatos++;
			}
						
			FileOutputStream fileout = new FileOutputStream("Excel.xls");
			book.write(fileout);
			fileout.close();
			
		}catch (FileNotFoundException ex) {
			Logger.getLogger(CsvWriterAppendExample.class.getName()).log(Level.SEVERE, null, ex);
		}catch (IOException ex) {
			Logger.getLogger(CsvWriterAppendExample.class.getName()).log(Level.SEVERE, null, ex);
		}catch (SQLException e) {
			Logger.getLogger(CsvWriterAppendExample.class.getName()).log(Level.SEVERE, null, e);
		}
	}

}

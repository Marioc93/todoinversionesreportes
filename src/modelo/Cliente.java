package modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Cliente{
	
	private Long id;
	private Usuario usuario;
	private String banco;
	private boolean hizoEncuesta;
	private String perfilInversor;
	

	public Cliente(){}
	
	public Cliente(Usuario usuario, String banco, boolean hizoEncuesta, String perfil) {
		this.usuario = usuario;
		this.banco = banco;
		this.hizoEncuesta = hizoEncuesta;
		this.perfilInversor = perfil;
	}
	
	@Id @GeneratedValue
    public Long getId() {
		return id;
    }

    private void setId(Long id) {
		this.id = id;
    }
    
    @OneToOne
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public boolean isHizoEncuesta() {
		return hizoEncuesta;
	}

	public void setHizoEncuesta(boolean hizoEncuesta) {
		this.hizoEncuesta = hizoEncuesta;
	}

	public String getPerfilInversor() {
		return perfilInversor;
	}

	public void setPerfilInversor(String perfilInversor) {
		this.perfilInversor = perfilInversor;
	}
    
    

}

package modelo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Producto {
	
	private Long id;
	private String nombre;
	private BigDecimal cotizacionVenta;
	private BigDecimal cotizacionCompra;
	private String descripcion;
	private int categoria;
	
	public Producto() {
		
	}

	@Id	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getCotizacionVenta() {
		return cotizacionVenta;
	}

	public void setCotizacionVenta(BigDecimal cotizacionVenta) {
		this.cotizacionVenta = cotizacionVenta;
	}

	public BigDecimal getCotizacionCompra() {
		return cotizacionCompra;
	}

	public void setCotizacionCompra(BigDecimal cotizacionCompra) {
		this.cotizacionCompra = cotizacionCompra;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	
	

}

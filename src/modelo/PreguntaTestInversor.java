package modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class PreguntaTestInversor {
	
	private Long id;
	private String pregunta;
	private String opcionA;
	private Integer puntajeOpcionA;
	private String opcionB;
	private Integer puntajeOpcionB;
	private String opcionC;
	private Integer puntajeOpcionC;
	
	public PreguntaTestInversor() {
		
	}
	
	@Id	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getOpcionA() {
		return opcionA;
	}

	public void setOpcionA(String opcionA) {
		this.opcionA = opcionA;
	}

	public Integer getPuntajeOpcionA() {
		return puntajeOpcionA;
	}

	public void setPuntajeOpcionA(Integer puntajeOpcionA) {
		this.puntajeOpcionA = puntajeOpcionA;
	}

	public String getOpcionB() {
		return opcionB;
	}

	public void setOpcionB(String opcionB) {
		this.opcionB = opcionB;
	}

	public Integer getPuntajeOpcionB() {
		return puntajeOpcionB;
	}

	public void setPuntajeOpcionB(Integer puntajeOpcionB) {
		this.puntajeOpcionB = puntajeOpcionB;
	}

	public String getOpcionC() {
		return opcionC;
	}

	public void setOpcionC(String opcionC) {
		this.opcionC = opcionC;
	}

	public Integer getPuntajeOpcionC() {
		return puntajeOpcionC;
	}

	public void setPuntajeOpcionC(Integer puntajeOpcionC) {
		this.puntajeOpcionC = puntajeOpcionC;
	}
	
	
	
}

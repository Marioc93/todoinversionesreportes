package modelo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Principal {
	
	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
		
	public static void crearCliente() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		System.out.println("Llego hasta ac�");
		
		Usuario u = new Usuario("pepe", "lopez", "pepe@mail.com", "upalala");
		Cliente c = new Cliente(u, "tu vieja", true, "Moderador");
		
		entityManager.persist(u);
		entityManager.persist(c);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public static void main(String[] args) {
		System.out.println("Usuario y cliente creado");
		crearCliente();
	}

}

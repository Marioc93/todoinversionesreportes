package modelo;

//import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import javax.persistence.OneToMany;

@Entity
public class Encuesta {
	
	private Long id;
//	private List<String> pregunta;
	private String respuesta;
	
	public Encuesta() {
		
	}
	
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
//	@OneToMany
//	public List<String> getPregunta() {
//		return pregunta;
//	}
//
//	public void setPregunta(List<String> pregunta) {
//		this.pregunta = pregunta;
//	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
}

package modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Usuario {
	
	private Long id;
	private String dni;
	private String nombre;
	private String apellido;
	private Date fechaNac;
	private String mail;
	private String cont;
	private boolean esCeo = false;
	private boolean esAdmin = false;
	
	public Usuario() {}
	
	public Usuario(String nombre, String apellido, String mail, String cont){
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.cont = cont;
	}
	
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCont() {
		return cont;
	}

	public void setCont(String cont) {
		this.cont = cont;
	}

	public boolean isEsCeo() {
		return esCeo;
	}

	public void setEsCeo(boolean esCeo) {
		this.esCeo = esCeo;
	}

	public boolean isEsAdmin() {
		return esAdmin;
	}

	public void setEsAdmin(boolean esAdmin) {
		this.esAdmin = esAdmin;
	}
	
	

}
